# Listado puertos utilizados por el servidor 
| Servico       | Puerto |
|---------------|--------|
| Cliente-web   | 3025   |
| ssh           | 9122   |
| Interfaz      | 3026   |
| Debug-cliente | 16440  |
| 3g-cliente    | 17000  |
| mysql         | 3303   |