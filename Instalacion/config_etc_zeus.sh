#!/bin/bash
# -*- ENCODING: UTF-8 -*-

YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

dirBase="/etc/zeus"
configName="zeus.conf"
debugName="console-debug.conf"

dirConfig="$dirBase/$configName"
dirDebug="$dirBase/$debugName"

echo " ======================================================="
echo " Instalacion de Archivos de Configuracion Servidor-Zeus";
echo " =======================================================";

sudo rm -f ${dirConfig}
sudo rm -f ${dirDebug}

if [ ! -d ${dirBase} ]; then
	sudo mkdir ${dirBase}
fi

sudo touch ${dirConfig}
sudo touch ${dirDebug}

echo "################################################" | sudo tee ${dirConfig}
echo "# Este es el archivo de configuracion del Servidor Zeus. Contiene" | sudo tee -a ${dirConfig}
echo "# las directivas de configuracion que se le dan al servidor." | sudo tee -a ${dirConfig}
echo "#######################################" | sudo tee -a ${dirConfig}
echo "# Escribir una configuracion por linea" | sudo tee -a ${dirConfig}
echo "# mantener Clasificacion [zeusServer] y pares llave=valor " | sudo tee -a ${dirConfig}
echo "#######################################" | sudo tee -a ${dirConfig}
echo "[zeusServer]" | sudo tee -a ${dirConfig}
echo "#legacy" | sudo tee -a ${dirConfig}
echo "#puertos=1" | sudo tee -a ${dirConfig}

echo "#configuracion del hardware ZeusServer"
echo "conexionSerial=1" | sudo tee -a ${dirConfig}
echo "puertosPorHilo=1" | sudo tee -a ${dirConfig}

echo "#configuracion del cliente escritorio y web"
echo "socket=3025" | sudo tee -a ${dirConfig}
echo "clientes=0" | sudo tee -a ${dirConfig}

echo "#configuracion del cliente debug"
echo "socketDebug=16440" | sudo tee -a ${dirConfig}
echo "clientesDebug=0" | sudo tee -a ${dirConfig}

echo "#configuracion del cliente 3G"
echo "socket3G=17000" | sudo tee -a ${dirConfig}
echo "clientes3G=0" | sudo tee -a ${dirConfig}
echo "clientes3Gextra=0" | sudo tee -a ${dirConfig}

echo "#configuracion del cliente 3G" | sudo tee -a ${dirConfig}
echo "#Sin supervision, puede ser enable o disable" | sudo tee -a ${dirConfig}
echo "sinSupervision=enable" | sudo tee -a ${dirConfig}



echo "###################################### " | sudo tee ${dirDebug}
echo "#Ingresar consolas virtuales en lineas diferentes " | sudo tee -a ${dirDebug}
echo "# No dejar lineas en blanco" | sudo tee -a ${dirDebug}
echo "#########################" | sudo tee -a ${dirDebug}
echo "#" | sudo tee -a ${dirDebug}
echo "#/dev/ttyUSB0" | sudo tee -a ${dirDebug}
echo "#/dev/ttyUSB1" | sudo tee -a ${dirDebug}
echo "#/dev/ttyUSB2" | sudo tee -a ${dirDebug}
echo "#/dev/ttyUSB3" | sudo tee -a ${dirDebug}
echo "##########################" | sudo tee -a ${dirDebug}
echo "# consolas extra" | sudo tee -a ${dirDebug}
echo "#/dev/pts/8" | sudo tee -a ${dirDebug}
echo "#/dev/pts/13" | sudo tee -a ${dirDebug}
echo "#/dev/pts/16" | sudo tee -a ${dirDebug}
echo "##########################" | sudo tee -a ${dirDebug}

echo " Fin Creacion de archivos .conf";
echo " =======================================================";
