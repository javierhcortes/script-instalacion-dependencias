#!/bin/bash
# -*- ENCODING: UTF-8 -*-

if [[ $(id -u) -eq 0 ]] ; then echo "EJECUTAR COMO USUARIO ZEUSBASE" ; exit 1 ; fi

user=$(whoami)
dirLocalBase="/home/$user"
nameLocalBase=".zeusServer"
fullDirLocal="$dirLocalBase/$nameLocalBase"

fileName="ListId.csv"
dirFile="$fullDirLocal/$fileName"

fileName2="productInfo.csv"
dirFile2="$fullDirLocal/$fileName2"

nameLOG="LOGZEUS"
dirLOG="$dirLocalBase/$nameLOG"


if [ ! -d ${fullDirLocal} ]; then
	mkdir -m 0775 ${fullDirLocal}	
fi

if [ ! -d ${dirLOG} ]; then
	mkdir -m 0775 ${dirLOG}
fi

echo "Instalacion de archivos de trabajo en ejecucion de Servidor-Zeus"
echo " ======================================================="

rm -f ${dirFile}
touch -f ${dirFile}
echo "1,2494" | tee ${dirFile}
echo "1,2495" | tee -a ${dirFile}

rm -f ${dirFile2}
touch -f ${dirFile2}
echo "6,3,3" | tee ${dirFile2}
echo "5,5,4" | tee -a ${dirFile2}
echo "4,3,3" | tee -a ${dirFile2}


echo " ======================================================="
echo "*) Creacion de dialout y config de usuarios"

