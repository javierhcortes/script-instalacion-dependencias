#!/bin/bash
# -*- ENCODING: UTF-8 -*-

YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

echo "=====================================";
echo " Herramientas de PC ciudad";

sudo apt-get -y install unzip && echo -e "${GRE} unzip OK${NC}" || echo -e "${RED} unzip Failed${NC}"
sudo apt-get -y install ethtool && echo -e "${GRE} ethtool OK${NC}" || echo -e "${RED} ethtool Failed${NC}"
sudo apt-get -y install uptimed && echo -e "${GRE} uptimed OK${NC}" || echo -e "${RED} uptimed Failed${NC}"
sudo apt-get -y install socat && echo -e "${GRE} socat OK${NC}" || echo -e "${RED} socat Failed${NC}"
echo "==========================================="

