#!/bin/bash
# -*- ENCODING: UTF-8 -*-

YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color


echo "========================================";
echo " 7 ) Instalacion	Mysql";
#sudo apt-get -y install libapache2-mod-auth-mysql && echo -e "${GRE} libapache2-mod-auth-mysql OK${NC}" || echo -e "${RED} libapache2-mod-auth-mysql Failed${NC}"
sudo apt-get install mysql-server && echo -e "${GRE} mysql-server OK${NC}" || echo -e "${RED} mysql-server Failed${NC}"


if [ -f "/etc/mysql/mysql.conf.d/mysqld.cnf" ]; then
	sudo sed -i 's/3306/3303/g' /etc/mysql/mysql.conf.d/mysqld.cnf && echo -e "${GRE} ver16- CAmbio de puerto a 3303 OK${NC}" || echo -e "${RED} ver16CAmbio de puerto a 3303 Failed${NC}"
	sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/mysql.conf.d/mysqld.cnf && echo -e "${GRE} CAmbio de bindAddress OK${NC}" || echo -e "${RED} CAmbio de bind-address Failed${NC}"

	echo "secure_file_priv=/tmp" | sudo tee -a /etc/mysql/mysql.conf.d/mysqld.cnf
	echo "sql_mode = \"NO_ZERO_IN_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION\" " | sudo tee -a /etc/mysql/mysql.conf.d/mysqld.cnf
	echo "event_scheduler=ON" | sudo tee -a /etc/mysql/mysql.conf.d/mysqld.cnf
fi;


