#!/bin/bash
# -*- ENCODING: UTF-8 -*-

YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

echo "=====================================";
echo " Instalacion librerias ZeusServer";

sudo apt install -y tzdata
sudo apt install -y build-essential
sudo apt install -y libmysql++-dev libmysql++3v5 libmysqlclient-dev zlib1g-dev
sudo apt install -y libssl-dev libssl-doc
sudo apt install -y libhpdf-dev libhpdf-2.3.0
sudo apt install -y libboost-dev
sudo apt install -y libserial-dev

echo "====================================="
echo " COn esto ya podriamos compilar e instalar ZeusServer"
