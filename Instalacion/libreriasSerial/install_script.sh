#!/bin/bash
# -*- ENCODING: UTF-8 -*-

sudo apt-get install libboost-all-dev
sudo apt-get install python-dev
tar -xzvf sip-4.18.tar.gz
unzip libserial-master.zip
cd sip-4.18/
python configure.py
make
sudo make install
cd ../libserial-master/
make -f Makefile.dist
./configure
make
sudo make install
cd ..
sudo cp libserialport.h /usr/local/include