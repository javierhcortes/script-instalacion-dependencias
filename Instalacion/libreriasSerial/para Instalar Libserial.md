### Antes de instalar LibSerial

1) sudo apt-get install libboost-all-dev
1.5) sudo apt-get install python-dev

2) Descomprimir SIP.4.18

		tar -xzvf sip-4.18.tar.gz

3) (Con python3 instalado) python configure.py

4) make

5) sudo make install

6) Descomprimir libserial

		unzip libserial-master.zip

7) make -f Makefile.dist

8) ./configure , make , sudo make install

9) Revisar /usr/local/include 4 archivos

-rw-r--r--  1 root root 57422 jun 17 17:00 libserialport.h
-rw-r--r--  1 root root 14419 jun 20 11:21 SerialPort.h
-rw-r--r--  1 root root 21069 jun 20 11:21 SerialStreamBuf.h
-rw-r--r--  1 root root 12345 jun 20 11:21 SerialStream.h


sudo apt-get install libboost-all-dev
sudo apt-get install python-dev
tar -xzvf sip-4.18.tar.gz
unzip libserial-master.zip
cd sip-4.18/
python configure.py
make
sudo make install
cd ../libserial-master/
make -f Makefile.dist
./configure
make
sudo make install
cd ..
sudo cp libserialport.h /usr/local/include