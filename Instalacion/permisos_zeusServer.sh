#!/bin/bash
# -*- ENCODING: UTF-8 -*-

YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

if [[ $(id -u) -eq 0 ]] ; then echo "EJECUTAR COMO USUARIO ZEUSBASE" ; exit 1 ; fi

echo "=====================================";
echo " Configuracion PC permisos (para ZeusServer)";

user=$(whoami)
sudo adduser $user dialout && echo -e "${GRE} AddUser OK${NC}" || echo -e "${RED} AddUSer Failed${NC}"
sudo update-rc.d -f ntpdate remove

echo "Fin dialout ";
