#!/bin/bash
# -*- ENCODING: UTF-8 -*-


wget https://www.syntevo.com/downloads/smartgit/smartgit-18_1_4.deb
sudo dpkg -i smartgit-18_1_4.deb
rm -rf smartgit-18_1_4.deb

wget https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb
sudo dpkg -i dbeaver-ce_latest_amd64.deb
rm -rf dbeaver-ce_latest_amd64.deb

wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt-get update
sudo apt-get install sublime-text

sudo apt install -y codeblocks codeblocks-contrib
sudo apt install -y dbviewer gimp krita inkscape
sudo apt install -y calibre
sudo apt install -y meld
sudo apt install -y dia
sudo apt install -y leafpad
sudo apt install -y dropbox
sudo apt install -y openshot
