## como habilitar conexion por ssh en linux

1) Con el gestor de paquetes apt-get

		sudo apt-get install openssh-server

2) cambiar el puerto por defecto de ssh

		sudo nano /etc/ssh/sshd_config

y editar linea
		#PORT 22
por
		Port 9122

3) reiniciar servicio de ssh

		/etc/init.d/ssh restart

4) Verificar que el servicio funciona

		ssh -p 9122 username@localhost


### Opcionales de seguridad

1) Establecer conexion por llave RSA en vez de user y password

[Link](http://askubuntu.com/questions/115151/how-to-setup-passwordless-ssh-access-for-root-user )
[Link2](http://www.linuxproblem.org/art_9.html )

2) Establecer port knocking en otro puerto


[Link](https://www.marksanborn.net/linux/add-port-knocking-to-ssh-for-extra-security/ )

3) desabilitar RemoteLogin Root de la configuracion de ssh


[Link](http://askubuntu.com/questions/27559/how-do-i-disable-remote-ssh-login-as-root-from-a-server)

4) desabilitar el Xforwading de la configuracion de ssh


[Link](https://www.google.com )
