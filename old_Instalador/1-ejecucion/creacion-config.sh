#!/bin/bash
# -*- ENCODING: UTF-8 -*-

YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

echo " =======================================================";
echo "3) Zona Horaria ";
sudo apt-get -y install tzdata tzdata-java

echo "zdump de la zona horaria-- comprobar estas definiciones (actualizado 2016/ post cambio horario)";
echo "";
echo -e "${YEL} zdump consola : ";

zdump -v America/Santiago | grep 2016

echo "";
echo -e "${GRE} zdump buscado : ";
echo "America/Santiago  Sun Apr 26 02:59:59 2015 UTC = Sat Apr 25 23:59:59 2015 CLST isdst=1 gmtoff=-10800";
echo "America/Santiago  Sun Apr 26 03:00:00 2015 UTC = Sun Apr 26 00:00:00 2015 CLT isdst=0 gmtoff=-10800";
echo "America/Santiago  Sun May 15 02:59:59 2016 UT = Sat May 14 23:59:59 2016 CLST isdst=1 gmtoff=-10800"
echo "America/Santiago  Sun May 15 03:00:00 2016 UT = Sat May 14 23:00:00 2016 CLT isdst=0 gmtoff=-14400"
echo "America/Santiago  Sun Aug 14 03:59:59 2016 UT = Sat Aug 13 23:59:59 2016 CLT isdst=0 gmtoff=-14400"
echo "America/Santiago  Sun Aug 14 04:00:00 2016 UT = Sun Aug 14 01:00:00 2016 CLST isdst=1 gmtoff=-10800"

echo -e "${NC}";

echo "Finalmente tenemos el timedatectl : ";
timedatectl status


echo "Fin Actualizacion Zona Horaria";
echo "=====================================";
echo "4) Instalacion librerias";

# REVISAR si zdump esta vacio
# http://wiki.synaptic.cl/wiki/HowTo_Cambio_de_hora_en_Chile_Marzo_de_2014
#
#

sudo apt-get -y install libserial-dev && echo -e "${GRE} Libserial OK${NC}" || echo -e "${RED} Libserial Failed${NC}"
sudo apt-get -y install libhpdf-2.2.1 && echo -e "${GRE} libhpdf OK${NC}" || echo -e "${RED} libhpdf Failed${NC}"
sudo apt-get -y install libhpdf-dev && echo -e "${GRE} libhpdf-dev OK${NC}" || echo -e "${RED} libhpdf-dev Failed${NC}"
sudo apt-get -y install libmysqlclient-dev && echo -e "${GRE} libmysqlclient OK${NC}" || echo -e "${RED} libmysqlclient Failed${NC}"
sudo apt-get -y install libmysqlclient20 && echo -e "${GRE} libmysqlclient18 OK${NC}" || echo -e "${RED} libmysqlclient18 Failed${NC}"
sudo apt-get -y install libmysqld-dev && echo -e "${GRE} libmysqld-dev OK${NC}" || echo -e "${RED} libmysqld-dev Failed${NC}"
sudo apt-get -y install libmysql++-dev && echo -e "${GRE} libmysql++ OK${NC}" || echo -e "${RED} libmysql++ Failed${NC}"
sudo apt-get -y install libmysql++3v5 && echo -e "${GRE} libmysql++3 OK${NC}" || echo -e "${RED} libmysql++3 Failed${NC}"
sudo apt-get -y install libssl-dev && echo -e "${GRE} libssl-dev OK${NC}" || echo -e "${RED} libssl-dev Failed${NC}"
sudo apt-get -y install openssl && echo -e "${GRE} openssl-dev OK${NC}" || echo -e "${RED} openssl-dev Failed${NC}"
sudo apt-get -y install libboost-all-dev && echo -e "${GRE} libboost-all-dev OK${NC}" || echo -e "${RED} libboost-all-dev Failed${NC}"

echo "Fin Instalacion librerias";
echo "=====================================";
echo "5) Creacion de dialout y config de usuarios";

sudo adduser $user dialout && echo -e "${GRE} AddUser OK${NC}" || echo -e "${RED} AddUSer Failed${NC}"
#sudo chmod -t /tmp && echo -e "${GRE} chmod /tmp OK${NC}" || echo -e "${RED} chmod /tmp Failed${NC}"
# sudo chmod a+rw /dev/ttyUSB0

#echo "Fin dialout ";

echo "========================================";
echo " 6 ) Instalacion Apache";

sudo apt-get update
sudo apt-get -y install apache2 && echo -e "${GRE} apache2 OK${NC}" || echo -e "${RED} apache2 Failed${NC}"

echo "========================================";
echo " 7 ) Instalacion	Mysql";
#sudo apt-get -y install libapache2-mod-auth-mysql && echo -e "${GRE} libapache2-mod-auth-mysql OK${NC}" || echo -e "${RED} libapache2-mod-auth-mysql Failed${NC}"
sudo apt-get -y install php7.0-mysql && echo -e "${GRE} php5-mysql OK${NC}" || echo -e "${RED} php5-mysql Failed${NC}"
sudo apt-get install mysql-server && echo -e "${GRE} mysql-server OK${NC}" || echo -e "${RED} mysql-server Failed${NC}"


if [ -f "/etc/mysql/mysql.conf.d/mysqld.cnf" ]; then
	sudo sed -i 's/3306/3303/g' /etc/mysql/mysql.conf.d/mysqld.cnf && echo -e "${GRE} ver16- CAmbio de puerto a 3303 OK${NC}" || echo -e "${RED} ver16CAmbio de puerto a 3303 Failed${NC}"
	sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/mysql.conf.d/mysqld.cnf && echo -e "${GRE} CAmbio de bindAddress OK${NC}" || echo -e "${RED} CAmbio de bind-address Failed${NC}"

	echo "secure_file_priv=/tmp" | sudo tee -a /etc/mysql/mysql.conf.d/mysqld.cnf
	echo "sql_mode = \"NO_ZERO_IN_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION\" " | sudo tee -a /etc/mysql/mysql.conf.d/mysqld.cnf
	echo "event_scheduler=ON" | sudo tee -a /etc/mysql/mysql.conf.d/mysqld.cnf
fi



#if [ -f "/etc/mysql/my.cnf" ]; then
	#sudo sed -i 's/3306/3303/g' /etc/mysql/my.cnf && echo -e "${GRE} ver14 CAmbio de puerto a 3303 OK${NC}" || echo -e "${RED} ver14 CAmbio de puerto a 3303 Failed${NC}"
	#sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/my.cnf && echo -e "${GRE} CAmbio de bindAddress OK${NC}" || echo -e "${RED} CAmbio de bind-address Failed${NC}"
#fi

echo "========================================";
echo "8) Instalacion PHP";

sudo apt-get -y install php7.0 && echo -e "${GRE} php5 OK${NC}" || echo -e "${RED} php5 Failed${NC}"
sudo apt-get -y install libapache2-mod-php7.0 && echo -e "${GRE} libapache2-mod-php5 OK${NC}" || echo -e "${RED} libapache2-mod-php5 Failed${NC}"
sudo apt-get -y install php7.0-mcrypt && echo -e "${GRE} php5-mcrypt OK${NC}" || echo -e "${RED} php5-mcrypt Failed${NC}"
sudo apt-get -y install mcrypt && echo -e "${GRE} mcrypt OK${NC}" || echo -e "${RED} mcrypt Failed${NC}"

#3.6) la base de datos debe usar el puerto 3303 de transferencia 
# en localhost, esto se modifica en /etc/mysql/my.cnf con 
#

echo "10) Instalacion de complementarios";
echo "Revisar este comandop ejecutandop uprecords";
sudo apt-get -y install uptimed && echo -e "${GRE} uptimed OK${NC}" || echo -e "${RED} uptimed Failed${NC}"
sudo apt-get -y install socat && echo -e "${GRE} socat OK${NC}" || echo -e "${RED} socat Failed${NC}" "==========================================="


echo "11) Herramientas de compilacion";
sudo apt-get -y install build-essential && echo -e "${GRE} build-essential OK${NC}" || echo -e "${RED} build-essential Failed${NC}"
sudo apt-get -y install unzip && echo -e "${GRE} unzip OK${NC}" || echo -e "${RED} unzip Failed${NC}"
sudo apt-get -y install dh-autoreconf && echo -e "${GRE} dh-autoreconf OK${NC}" || echo -e "${RED} dh-autoreconf Failed${NC}"
sudo apt-get -y install ethtool && echo -e "${GRE} ethtool OK${NC}" || echo -e "${RED} ethtool Failed${NC}"

#sudo apt install --no-install-recommends lubuntu-core
#sudo apt install lubuntu-desktop
sudo apt install lxdm

sudo apt-get install lubuntu-desktop

echo "12) algunos check "

sudo update-rc.d -f ntpdate remove

#ethtool para comprobar si hay Link en la red (conectado fisicamente)

#echo "================================================";
#echo "12) CAmbios en el grub";


#13) abrir una consola y ejecutar sudo gedit /boot/grub/grub.cfg
#-Buscar la linea 57 o donde diga "${recordfail} = 1" ; then
#-donde diga set timeout, setear =5 o =10;


#14) Comprobar en consola : gedit /etc/default/grub
#Este es el encabezado, GRUB_TIMEOUT=10 es la linea importante...

#sudo update-grub

#- no olivadr cargar triggers en base de datos nueva
# los view y las cuentas RemoteRoot, ServerUser

#http://askubuntu.com/questions/372164/how-to-load-ubuntu-server-automatically-in-grub
