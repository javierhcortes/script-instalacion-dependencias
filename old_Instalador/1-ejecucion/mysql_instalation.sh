### Este archivo solo es para la instalacion de MySql - incluido en creacion-config.sh

echo " 7 ) Instalacion	Mysql";
sudo apt-get -y install ibapache2-mod-php7.0 && echo -e "${GRE} ibapache2-mod-php7.0 OK${NC}" || echo -e "${RED} ibapache2-mod-php7.0 Failed${NC}"
sudo apt-get -y install php7.0-mysql && echo -e "${GRE} php5-mysql OK${NC}" || echo -e "${RED} php5-mysql Failed${NC}"
sudo apt-get install mysql-server && echo -e "${GRE} mysql-server OK${NC}" || echo -e "${RED} mysql-server Failed${NC}"

#sudo mysql_install_db
sudo /usr/bin/mysql_secure_installation


if [ -f "/etc/mysql/mysql.conf.d/mysqld.cnf" ]; then
	sudo sed -i 's/3306/3303/g' /etc/mysql/mysql.conf.d/mysqld.cnf && echo -e "${GRE} CAmbio de puerto a 3303 OK${NC}" || echo -e "${RED} CAmbio de puerto a 3303 Failed${NC}"
	sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/mysql.conf.d/mysqld.cnf && echo -e "${GRE} CAmbio de bindAddress OK${NC}" || echo -e "${RED} CAmbio de bind-address Failed${NC}"
	echo "secure_file_priv=/tmp" | sudo tee -a /etc/mysql/mysql.conf.d/mysqld.cnf
	echo "sql_mode = \"NO_ZERO_IN_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION\" " | sudo tee -a /etc/mysql/mysql.conf.d/mysqld.cnf
	echo "event_scheduler=ON" | sudo tee -a /etc/mysql/mysql.conf.d/mysqld.cnf
fi

if [ -f "/etc/mysql/my.cnf" ]; then
	sudo sed -i 's/3306/3303/g' /etc/mysql/my.cnf && echo -e "${GRE} CAmbio de puerto a 3303 OK${NC}" || echo -e "${RED} CAmbio de puerto a 3303 Failed${NC}"
	sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/my.cnf && echo -e "${GRE} CAmbio de bindAddress OK${NC}" || echo -e "${RED} CAmbio de bind-address Failed${NC}"
fi
echo "========================================";


#CREATE USER 'nombre_usuario'@'localhost' IDENTIFIED BY 'tu_contrasena';
#GRANT ALL PRIVILEGES ON * . * TO 'nombre_usuario'@'localhost';
#FLUSH PRIVILEGES;
