#!/bin/bash
# -*- ENCODING: UTF-8 -*-

YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

dirBase="/etc/zeus"
configName="zeus.conf"
debugName="console-debug.conf"

dirConfig="$dirBase/$configName"
dirDebug="$dirBase/$debugName"

dirBinaray="/usr/local/bin"
binName="ZeusServer2"
dirBin="$dirBinary/binName"

dirLocalBaseRoot="/root"
nameLocalBase=".zeusServer"
fileName="ListId.csv"

fullDirLocalRoot="$dirLocalBaseRoot/$nameLocalBase"
dirFileRoot="$fullDirLocalRoot/$fileName"

fileName2="productInfo.csv"
dirFileRoot2="$fullDirLocalRoot/$fileName2"

dirLocalBase="/root"
nameLOG="LOGZEUS"
dirLOG="$dirLocalBase/$nameLOG"

nameRun="run"
dirRun="$dirLocalBase/$nameRun"

echo " ======================================================="
echo " Instalacion de Archivos de Configuracion Servidor-Zeus";
echo " =======================================================";

echo "1 ) Creacion de archivos .conf";

sudo rm -f ${dirConfig}
sudo touch ${dirConfig}
echo "################################################" | sudo tee ${dirConfig}
echo "# Este es el archivo de configuracion del Servidor Zeus. Contiene" | sudo tee -a ${dirConfig}
echo "# las directivas de configuracion que se le dan al servidor." | sudo tee -a ${dirConfig}
echo "#######################################" | sudo tee -a ${dirConfig}
echo "# Escribir una configuracion por linea" | sudo tee -a ${dirConfig}
echo "# mantener Clasificacion [zeusServer] y pares llave=valor " | sudo tee -a ${dirConfig}
echo "#######################################" | sudo tee -a ${dirConfig}
echo "[zeusServer]" | sudo tee -a ${dirConfig}
echo "puertos=1" | sudo tee -a ${dirConfig}
echo "puertosPorHilo=1" | sudo tee -a ${dirConfig}
echo "clientes=5" | sudo tee -a ${dirConfig}
echo "socket=3025" | sudo tee -a ${dirConfig}
echo "clientesDebug=3" | sudo tee -a ${dirConfig}
echo "socketDebug=16440" | sudo tee -a ${dirConfig}
echo "macName=eth0" | sudo tee -a ${dirConfig}

## MACNAME Tiene QUE SER DEPENDIENTE DEL SISTEMA

if [ ! -d ${dirBase} ]; then
	sudo mkdir ${dirBase}
fi

if [ ! -d ${dirLOG} ]; then
	sudo mkdir -m 0775 ${dirLOG}
fi

if [ ! -d ${dirRun} ]; then
	sudo mkdir -m 0775 ${dirRun}
fi

sudo rm -f ${dirDebug}
sudo touch -f ${dirDebug}
echo "###################################### " | sudo tee ${dirDebug}
echo "#Ingresar consolas virtuales en lineas diferentes " | sudo tee -a ${dirDebug}
echo "# No dejar lineas en blanco" | sudo tee -a ${dirDebug}
echo "#########################" | sudo tee -a ${dirDebug}
echo "#" | sudo tee -a ${dirDebug}
echo "#/dev/ttyUSB0" | sudo tee -a ${dirDebug}
echo "#/dev/ttyUSB1" | sudo tee -a ${dirDebug}
echo "#/dev/ttyUSB2" | sudo tee -a ${dirDebug}
echo "#/dev/ttyUSB3" | sudo tee -a ${dirDebug}
echo "##########################" | sudo tee -a ${dirDebug}
echo "# consolas extra" | sudo tee -a ${dirDebug}
echo "#/dev/pts/8" | sudo tee -a ${dirDebug}
echo "#/dev/pts/13" | sudo tee -a ${dirDebug}
echo "#/dev/pts/16" | sudo tee -a ${dirDebug}
echo "##########################" | sudo tee -a ${dirDebug}

echo " Fin Creacion de archivos .conf";
echo " =======================================================";
echo "2) Copia de Archivo ErrorCodeList.txt";

if [ -f "ErrorCodeList.txt" ]; then
   	sudo cp ErrorCodeList.txt ${dirBase}/
   
	if [ $? -eq 0 ]; then
	 echo -e "${GRE} Fin Copia de Archivo ErrorCodeList.txt ----> OK ${NC}";
	else
	 echo -e "${RED} Fin Copia de Archivo ErrorCodeList.txt ----> FAIL ${NC}";
	fi
else
   echo -e "${RED} Fin Copia de Archivo ErrorCodeList.txt ----> FAIL ${NC}";
fi

echo "3) Archivos personales de ZeusServer-root"
echo " ======================================================="

if [ ! -d ${fullDirLocalRoot} ]; then
	sudo mkdir ${fullDirLocalRoot}
fi

sudo rm -f ${dirFileRoot}
sudo touch -f ${dirFileRoot}
echo "1,2494" | sudo tee ${dirFileRoot}
echo "1,2495" | sudo tee -a ${dirFileRoot}

sudo rm -f ${dirFileRoot2}
sudo touch -f ${dirFileRoot2}
echo "6,3,3" | sudo tee ${dirFileRoot2}
echo "5,5,4" | sudo tee -a ${dirFileRoot2}
echo "4,3,3" | sudo tee -a ${dirFileRoot2}

echo "========================================";
echo "9) Copia Ejecutable en $dirBin";

if [ -f ${binName} ]; then
	sudo cp ${binName} ${dirBin}
	if [ $? -eq 0 ]; then
	 echo -e "${GRE} Fin Copia de Archivo $binName ----> OK ${NC}";
	else
	 echo -e "${RED} Fin Copia de Archivo $binName ----> FAIL ${NC}";
	fi
else
   echo -e "${RED} Fin Copia de Archivo $binName ----> FAIL ${NC}";
fi

echo "FIn copia Ejecutable en $binName";
echo "=======================================";


#sudo chmod -t /tmp && echo -e "${GRE} chmod /tmp OK${NC}" || echo -e "${RED} chmod /tmp Failed${NC}"
#sudo chmod a+rw /dev/ttyUSB0
