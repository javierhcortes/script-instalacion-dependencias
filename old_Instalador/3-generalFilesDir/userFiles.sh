#!/bin/bash

user=$(whoami)
dirLocalBase="/home/$user"
nameLocalBase=".zeusServer"
fullDirLocal="$dirLocalBase/$nameLocalBase"

fileName="ListId.csv"
dirFile="$fullDirLocal/$fileName"

fileName2="productInfo.csv"
dirFile2="$fullDirLocal/$fileName2"

nameLOG="LOGZEUS"
#dirLOG="$dirLocalBase/$nameLOG"

nameRun="run"
dirRun="$dirLocalBase"

if [ ! -d ${fullDirLocal} ]; then
	mkdir -m 0775 ${fullDirLocal}	
fi

if [ ! -d ${dirLOG} ]; then
	mkdir -m 0775 ${dirLOG}
fi

if [ ! -d ${fullDirLocal}/${nameRun} ]; then
	mkdir -m 0775 ${fullDirLocal}/${nameRun}
fi

echo "Instalacion de archivos personales de Servidor-Zeus"
echo " ======================================================="

rm -f ${dirFile}
touch -f ${dirFile}
echo "1,2494" | tee ${dirFile}
echo "1,2495" | tee -a ${dirFile}

rm -f ${dirFile2}
touch -f ${dirFile2}
echo "6,3,3" | tee ${dirFile2}
echo "5,5,4" | tee -a ${dirFile2}
echo "4,3,3" | tee -a ${dirFile2}


echo " ======================================================="
echo "*) Creacion de dialout y config de usuarios";

sudo adduser $user dialout && echo -e "${GRE} AddUser OK${NC}" || echo -e "${RED} AddUSer Failed${NC}"
sudo adduser root dialout && echo -e "${GRE} AddUser OK${NC}" || echo -e "${RED} AddUSer Failed${NC}"
echo "Fin dialout ";
