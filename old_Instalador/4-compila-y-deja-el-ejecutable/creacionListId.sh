#!/bin/bash
# -*- ENCODING: UTF-8 -*-

YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

dirBase="/etc/zeus"
configName="zeus.conf"
debugName="console-debug.conf"

dirConfig="$dirBase/$configName"
dirDebug="$dirBase/$debugName"

dirBinaray="/usr/local/bin"
binName="ZeusServer2"
dirBin="$dirBinaray/$binName"

user=$(whoami)
dirLocalBase="/home/$user"
nameLocalBase=".zeusServer"
fullDirLocal="$dirLocalBase/$nameLocalBase"

fileName="ListId.csv"
dirFile="$fullDirLocal/$fileName"

dirLocalBaseRoot="/root"
fullDirLocalRoot="$dirLocalBaseRoot/$nameLocalBase"
dirFileRoot="$fullDirLocalRoot/$fileName"

if [ ! -d ${fullDirLocal} ]; then
	mkdir ${fullDirLocal}
fi


echo " Fin Creacion de archivos .conf";
echo " =======================================================";
echo "2) Copia de Archivo ErrorCodeList.txt";

if [ -f "ErrorCodeList.txt" ]; then
   	sudo cp ErrorCodeList.txt ${dirBase}
   
	if [ $? -eq 0 ]; then
	 echo -e "${GRE} Fin Copia de Archivo ErrorCodeList.txt ----> OK ${NC}";
	else
	 echo -e "${RED} Fin Copia de Archivo ErrorCodeList.txt ----> FAIL ${NC}";
	fi
else
   echo -e "${RED} Fin Copia de Archivo ErrorCodeList.txt ----> FAIL ${NC}";
fi

echo "========================================";
echo "9) Copia Ejecutable en $dirBin";

if [ -f ${binName} ]; then
	sudo cp ${binName} ${dirBin}
	if [ $? -eq 0 ]; then
	 echo -e "${GRE} Fin Copia de Archivo $binName ----> OK ${NC}";
	else
	 echo -e "${RED} Fin Copia de Archivo $binName ----> FAIL ${NC}";
	fi
else
   echo -e "${RED} Fin Copia de Archivo $binName ----> FAIL ${NC}";
fi

echo "FIn copia Ejecutable en $binName";
echo "=======================================";
