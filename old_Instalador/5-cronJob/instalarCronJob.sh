#!/bin/bash
# -*- ENCODING: UTF-8 -*-

YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

#user=$(whoami)
#echo -e "${RED}  Overwrite user crontab  ${NC}"
#crontab -u $user cronjobUser && echo -e "${GRE} crojob OK${NC}" || echo -e "${RED} cronjob Failed${NC}"
#echo -e "${RED}  Mostrando user crontab actual  ${NC}"


echo -e "${RED}  Overwrite sudo crontab  ${NC}"
sudo crontab -u root cronjob && echo -e "${GRE} crojob OK${NC}" || echo -e "${RED} cronjob Failed${NC}"
echo -e "${RED}  Mostrando sudo crontab actual  ${NC}"
sudo crontab -l
