#!/bin/bash
# -*- ENCODING: UTF-8 -*-

YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

user=$(whoami)
dirLocalBase="/home/$user"

if [ $user == 'root' ]; then
	echo "porfavor ejecute este archivo sin ser root"
	exit 1
fi

echo "Ingrese nombre Base-Datos : Zeus..."
read input_dbname
echo "Nombre Ingresado: Zeus$input_dbname"

while true; do
    read -p "Esta seguro de este nombre?: Zeus$input_dbname. y or n : " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Ingrese yes o no.";;
    esac
done

sudo chmod 755 /usr/local/bin/ZeusServer2

sudo cp miniDaemon.sh /usr/local/bin/
sudo chmod +x /usr/local/bin/miniDaemon.sh

sed -i "s/DBNAME=None/DBNAME=Zeus${input_dbname}/g" upServer.sh && echo -e "${GRE} replaced ZeusName OK${NC}" || echo -e "${RED} replaced ZeusName ${NC}"

sudo cp upServer.sh	/usr/local/bin && echo -e "${GRE} cp upServer OK${NC}" || echo -e "${RED} cp upServer ${NC}"
sudo cp seeServer.sh /usr/local/bin && echo -e "${GRE} cp seeServer OK${NC}" || echo -e "${RED} cp seeServer ${NC}"

sed -i "s/DBNAME=Zeus${input_dbname}/DBNAME=None/g" upServer.sh && echo -e "${GRE} replaced ZeusName OK${NC}" || echo -e "${RED} replaced ZeusName ${NC}"

if [ ! -d "${dirLocalBase}/.config/autostart/" ]; then
	mkdir -m 0775 "${dirLocalBase}/.config/autostart/"
fi

cp ZeusDesktop.desktop "${dirLocalBase}/.config/autostart/" && echo -e "${GRE} instaled AUTOSTART OK${NC}" || echo -e "${RED} not instaled AUTOSTART OK ${NC}"


#sudo echo "" > /etc/lightdm/lightdm.conf

echo "[SeatDefaults]" | sudo tee /etc/lightdm/lightdm.conf
echo "autologin-user=$user" | sudo tee -a /etc/lightdm/lightdm.conf
echo "autologin-user-timeout=0" | sudo tee -a /etc/lightdm/lightdm.conf
echo "user-session=Lubuntu" | sudo tee -a /etc/lightdm/lightdm.conf
echo "greeter-session=lightdm-gtk-greeter" | sudo tee -a /etc/lightdm/lightdm.conf