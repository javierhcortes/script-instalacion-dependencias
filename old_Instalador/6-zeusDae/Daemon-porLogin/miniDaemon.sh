#!/bin/bash
# -*- ENCODING: UTF-8 -*-
YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# PATHS
BIN=ZeusServer2
EXEC=upServer.sh
BINFILE=/usr/local/bin/$EXEC

fecha=`date +"%d-%m-%Y"`

while true
  do
    if [ $(pidof -x $BIN | wc -w) -eq 0 ]; then
      echo "Sin servidor - iniciando"
      lxterminal -T "Server-Run" -e $BINFILE
    fi

    if [ $(pidof -x $BIN | wc -w) -eq 1 ]; then
      ID=$(pidof -x $BIN)
      uid=$(awk '/^Uid:/{print $2}' /proc/$ID/status)
      user=$(getent passwd "$uid" | awk -F: '{print $1}')
      echo -e "${RED} ============================ ${NC}" $fecha
      echo -e "${RED}El usuario : " $user " esta ejecutando el server ${NC}"
      echo -e "${RED} ============================ ${NC}"
    fi

    if [ $(pidof -x $BIN | wc -w) -gt 1 ]; then
      echo "Mas de un servidor"
      echo "Con PIDS" $(pidof -x $EXEC)
      
      max=$(pidof -x -s $EXEC)
      for i in $(pidof -x $EXEC)
        do
        if [ $i -gt $max ]; then
          max=$i
        fi
      done

      kill $max
      echo "Servidor extra cerrado PID=" $max
    fi
  sleep 10
done
