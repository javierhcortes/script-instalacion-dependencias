#!/bin/bash
# -*- ENCODING: UTF-8 -*-

YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# PATHS
EXEC=ZeusServer2
nameLOG="LOGZEUS"
# var
#user=$(whoami)
## Run

echo -e "${GRE}============RUN SEE SERVER =====================${NC}"

if [ $(pidof -x $EXEC | wc -w) -eq 0 ]; then
	echo "No hay servidor ejecutandose..." 
	echo "para iniciar ejecute 'sudo service zeusDae start'"
	echo "o upServer.sh"
	exit 1
else
	ID=$(pidof -x $EXEC)
	uid=$(awk '/^Uid:/{print $2}' /proc/$ID/status)
	user=$(getent passwd "$uid" | awk -F: '{print $1}')
	echo -e "${RED}El usuario : " $user " esta ejecutando el server ${NC}"
	echo -e "${RED} ============================ ${NC}"

	## Revisar if [ -r 'root/LogZeus']

	if [ $user == 'root' ]; then
		dirLocalBase="/root"
	else
		dirLocalBase="/home/$user"
	fi

	dirLOG="${dirLocalBase}/${nameLOG}"
	echo "${dirLOG}"

	if [ -r ${dirLOG} ]; then 
		
		amountLogs=$(ls ${dirLOG} | wc -l )
		lastFile=$(ls -Art ${dirLOG} | tail -n 1)

		if [ $amountLogs -eq 0 ]; then
			echo "No hay Archivos de log almacenados"
			exit 1
		else
			tail -n 2000 -f "$dirLOG/$lastFile"
		fi
	
	else
		echo -e "${RED} No posee permisos de lectura para ${dirLOG} ${NC}"
		exit 1
	fi
fi

