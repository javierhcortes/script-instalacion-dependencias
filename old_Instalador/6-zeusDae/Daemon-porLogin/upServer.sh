#!/bin/bash

# PATHS
EXEC=ZeusServer2
DBNAME=None
nameLOG="LOGZEUS"

user=$(whoami)
if [ $user == 'root' ]; then
	dirLocalBase="/root"
else
	dirLocalBase="/home/$user"
fi
dirLOG="$dirLocalBase/$nameLOG"
BINFILE=/usr/local/bin/$EXEC

if [ ! -d ${dirLOG} ]; then
	mkdir -m 0775 ${dirLOG}
fi

fecha=`date +"%d-%m-%Y"`

if [ $(pidof -x $EXEC | wc -w) -eq 0 ]; then
	echo "fecha->" $fecha
	echo "Sin servidor - iniciando en $DBNAME"

	amountLogs=$(ls ${dirLOG} | wc -l )
	lastFile=$(ls -Art ${dirLOG} | tail -n 1)

	if [ $amountLogs -eq 0 ]; then
	  echo "0 files"

	  touch "$dirLOG/$fecha".txt
	  chmod 777 "$dirLOG/$fecha".txt
	  echo "===============================" >> "$dirLOG/$fecha".txt
	  echo "Servidor levantado " >> "$dirLOG/$fecha".txt
	  echo "===============================" >> "$dirLOG/$fecha".txt
	  $BINFILE "$DBNAME" | tee -a "$dirLOG/$fecha".txt
	else
	  echo ">0 files"
	  #echo "Last File : " ${lastFile}
	  #echo "date : " ${fecha}.txt
	  # analizar caso lastFile != fecha

	  if [ "${lastFile}" == "${fecha}.txt" ]; then
	  	echo "iguales en nombre y fecha"
	  	echo "===============================" >> "$dirLOG/$fecha".txt
	  	echo "Servidor levantado " >> "$dirLOG/$fecha".txt
	  	echo "===============================" >> "$dirLOG/$fecha".txt
	  	$BINFILE "$DBNAME" | tee -a "$dirLOG/$lastFile"

	  else
	  	echo "diferentes en nombre y fecha"
	  	touch "$dirLOG/$fecha".txt
	  	chmod 777 "$dirLOG/$fecha".txt
	  	echo "===============================" >> "$dirLOG/$fecha".txt
	  	echo "Servidor levantado " >> "$dirLOG/$fecha".txt
	  	echo "===============================" >> "$dirLOG/$fecha".txt
	  	$BINFILE "$DBNAME" | tee -a "$dirLOG/$fecha".txt
	  fi
	fi
else
	ID=$(pidof -x $EXEC)
	uid=$(awk '/^Uid:/{print $2}' /proc/$ID/status)
	echo "El usuario :"
	getent passwd "$uid" | awk -F: '{print $1}'
	echo "Esta ejecutando el server"
	echo "No se pudo iniciar"
fi
