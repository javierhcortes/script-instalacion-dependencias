#!/bin/bash
# -*- ENCODING: UTF-8 -*-

YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

EXEC=ZeusServer2
BINFILE=/usr/local/bin/$EXEC

user=$(whoami)
dirLocalBase="/home/$user"

if [ $user == 'root' ]; then
	echo "porfavor ejecute este archivo sin ser root"
	exit 1
fi

# check si existe ambos archivos en dir actual
 if [ -f "daemon_0.6.4-2_amd64.deb" -a -f "zeusDae" -a -f $BINFILE -a -f "upServer.sh" -a -f "seeServer.sh" ]; then
 	echo -e "${GRE} archivos necesarios OK ${NC}"
 else 
 	echo -e "${RED} faltan archivos para instalacion ${NC}"
 	exit 1
 fi

dirRun="/home/$user/.zeusServer/run"

sudo dpkg -i daemon_0.6.4-2_amd64.deb && echo -e "${GRE} instalado daemon handler OK${NC}" || echo -e "${RED} error daemon handler ${NC}"
sudo chmod 755 $BINFILE && echo -e "${GRE} zeusServer2 755 OK${NC}" || echo -e "${RED} zeusServer2 755 FAIL ${NC}"


sed -i "s#pidfiles=/var/run#pidfiles=${dirRun}#g" zeusDae && echo -e "${GRE} replaced zeusDae->run OK${NC}" || echo -e "${RED} replaced zeusDae->run fail ${NC}"
	chmod +x zeusDae && echo -e "${GRE} zeusDae +x OK${NC}" || echo -e "${RED} zeusDae +x FAIL ${NC}"
	# debe ser desde un archivo group writable
	sudo cp zeusDae $dirLocalBase && echo -e "${GRE} cp zeusDae OK${NC}" || echo -e "${RED} cp zeusDae FAIL ${NC}"
	sudo cp $dirLocalBase/zeusDae /etc/init.d/ && echo -e "${GRE} cp zeusDae -> init.d OK${NC}" || echo -e "${RED} cp zeusDae -> init.d FAIL ${NC}"
sed -i "s#pidfiles=${dirRun}#pidfiles=/var/run#g" zeusDae && echo -e "${GRE} replaced zeusDae->default OK${NC}" || echo -e "${RED} replaced zeusDae->default ${NC}"



echo "Ingrese nombre Base-Datos : Zeus..."
read input_dbname
echo "Nombre Ingresado: Zeus$input_dbname"

while true; do
    read -p "Esta seguro de este nombre?: Zeus$input_dbname. y or n : " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Ingrese yes o no.";;
    esac
done


#cambiamos upServer para la instalacion
sed -i "s/DBNAME=None/DBNAME=Zeus${input_dbname}/g" upServer.sh && echo -e "${GRE} replaced ZeusName->current OK${NC}" || echo -e "${RED} replaced ZeusName->current ${NC}"
	sudo cp upServer.sh	/usr/local/bin && echo -e "${GRE} cp upServer OK${NC}" || echo -e "${RED} cp upServer ${NC}"
	sudo cp seeServer.sh /usr/local/bin && echo -e "${GRE} cp seeServer OK${NC}" || echo -e "${RED} cp seeServer ${NC}"
	#restablecemos nombre por si hay que reinstalar
sed -i "s/DBNAME=Zeus${input_dbname}/DBNAME=None/g" upServer.sh && echo -e "${GRE} replaced ZeusName->default OK${NC}" || echo -e "${RED} replaced ZeusName->default ${NC}"

# probando comando

#/etc/init.d/zeusDae status
