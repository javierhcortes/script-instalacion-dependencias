#!/bin/bash
: '
# -*- ENCODING: UTF-8 -*-
*** Documentacion Serverup.sh *****
Archivo  = serverup.sh
Lenguaje = Bash
Propietario = mythosserver
Usuario = mythosserver
permisos = 777 - solo es necesario permisos de lectura y ejecucion por parte del usuario que lo inicia.
Requisitos = Ubicar ejecutable en carpeta junto con el script con permisos de ejecucion de parte del usuario que lo invoca.

Inicio manual = Se debe ir a la carpeta contenedora, abrir una terminal y ejecutar la linea ./serverup.sh
esto abrira una terminal con salida de texto informada por eventos ( inicio, levanta server , mas de un server)

Inicio automatico = Se debe cargar en las start up applications. esto hará que quede en modo background y se levante MythosServer
cuando es neesario. Esto se consigue agregando la linea /{UBICACION}/serverup.sh a la interfaz de gnome-session-properties

Depuracion de errores Modo Automatico = Para ver los echo del script llamar a una consola que abra el script en bash igual que el
modo automatico, esta vez ejecutando la linea gnome-terminal -e /{ubicacion}/serverup.sh & 

****
'
# PATHS
EXEC=Servidor-Quintero
NAME=upDownServerCh
OWNER=zeus
SCRPDIR=/home/zeus

# PAra atrapar las señales de parada y kill
#trap salida SIGHUP SIGINT SIGTERM SIGKILL

while true
fecha=`date`
#fecha = `date +%Y%m%d`
    do
     if [ $(pidof -x $EXEC | wc -w) -eq 0 ]
        then
        echo "fecha->" $fecha # 2>&1 | tee -a $LOGDIR_F
        echo "Sin servidor - iniciando"  #2>&1 | tee -a $LOGDIR_F
       
        #cd "$SCRPDIR"
        echo $(pwd) #2>&1 | tee -a $LOGDIR_F
	xterm -T "Servidor-Quintero" -e "./Servidor-Quintero 5 3025 Quintero-local >>.log_file.bin 2>&1"
     fi

     if [ $(pidof -x $EXEC | wc -w) -eq 1 ]
        then
        echo "Un servidor ejecutandose con PID" $(pidof -x $EXEC) # 2>&1
     fi

     if [ $(pidof -x $EXEC | wc -w) -gt 1 ]
        then
            echo "fecha->" $fecha #2>&1 | tee -a $LOGDIR_F
            echo "Mas de un servidor" #2>&1 | tee -a $LOGDIR_F
            echo "Con PIDS" $(pidof -x $EXEC) #2>&1 | tee -a $LOGDIR_F
            max=$(pidof -x -s $EXEC)

            for i in $(pidof -x $EXEC)
            do
                if [ $i -gt $max ]
                then
                    max=$i
                fi
            done

            kill $max
            echo "Servidor extra cerrado PID=" $max #2>&1 | tee -a $LOGDIR_F
    fi
      sleep 10
done